/*
 * Standard atmosphere calculator | ISA
 * author: atomw81
 * Default units: m
 * Default altitude type: geopotential
 */
import java.awt.EventQueue;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Color;
import javax.swing.JRadioButtonMenuItem;

public class calcApp {
// Initializing variables...
	private JFrame frame;
	private JTextField textField;
	private JRadioButton rdbtnM;
	private JRadioButton rdbtnFt;
	private JRadioButton rdbtnGeometric;
	private JRadioButton rdbtnGeopotential;
	private JLabel lblPressure;
	private JLabel label;
	private JLabel lblTemperature;
	private JLabel label_1;
	private JLabel lblDensity;
	private JLabel label_2;
	private JLabel lblAltitude;
	private JLabel lblAt;
	private JLabel lblAltLabel;
	private ButtonGroup bg = new ButtonGroup();
	private ButtonGroup bg2 = new ButtonGroup();
	private JLabel lblAtomsphericCalculator;
	private JLabel lblNewLabel;
	private JLabel lblPressurepa;
	private JLabel lblTemperaturek;
	private JLabel lblDensityKgm;
	private JLabel lblSpeedOfSound;
	private JLabel lblSpeedOfSound_1;
	private JLabel speedOfsound;
	String p_0,t_0,d_0,p,t,d,ss,s,NA="NA",un="";
	double T,P,D,h,Re=6371000,g=9.80665,gm=1.4,R=287.057,t0=288.15,p0=101325,d0=1.225,a1=-0.0065,a2=0.001,a3=0.0028,a4=-0.0028,a5=-0.002,a=0.0,p1,p2,p3,p4,p5,p6,t1,t2,t3,t4,t5,t6,d1,d2,d3,d4,d5,d6;
	int c=0;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calcApp window = new calcApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public calcApp() {
		initialize();
	}
    
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 561, 539);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		rdbtnM = new JRadioButton("m");
		rdbtnM.setFont(new Font("Tahoma", Font.PLAIN, 11));
		rdbtnM.setBackground(Color.LIGHT_GRAY);
		rdbtnM.setForeground(Color.BLACK);
		rdbtnM.setSelected(true);
		rdbtnM.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				appCal();
			}
		});
		rdbtnM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		rdbtnM.setBounds(356, 86, 43, 20);
		frame.getContentPane().add(rdbtnM);
			
		rdbtnFt = new JRadioButton("ft");
		rdbtnFt.setFont(new Font("Tahoma", Font.PLAIN, 11));
		rdbtnFt.setBackground(Color.LIGHT_GRAY);
		rdbtnFt.setForeground(Color.BLACK);
		rdbtnFt.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				appCal();
		    }
		});
		rdbtnFt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		rdbtnFt.setBounds(419, 86, 43, 21);
		frame.getContentPane().add(rdbtnFt);
		bg.add(rdbtnM);
		bg.add(rdbtnFt);
		
		rdbtnGeometric = new JRadioButton("Geometric");
		rdbtnGeometric.setFont(new Font("Tahoma", Font.PLAIN, 11));
		rdbtnGeometric.setBackground(Color.LIGHT_GRAY);
		rdbtnGeometric.setForeground(Color.BLACK);
		rdbtnGeometric.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				appCal();
			}
		});
		rdbtnGeometric.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		rdbtnGeometric.setBounds(96, 136, 109, 23);
		frame.getContentPane().add(rdbtnGeometric);
		
		rdbtnGeopotential = new JRadioButton("Geopotential");
		rdbtnGeopotential.setFont(new Font("Tahoma", Font.PLAIN, 11));
		rdbtnGeopotential.setSelected(true);
		rdbtnGeopotential.setBackground(Color.LIGHT_GRAY);
		rdbtnGeopotential.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				appCal();
			}
		});
		rdbtnGeopotential.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		rdbtnGeopotential.setBounds(96, 110, 109, 23);
		frame.getContentPane().add(rdbtnGeopotential);
		bg2.add(rdbtnGeometric);
		bg2.add(rdbtnGeopotential);
		
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				appCal();
			}
		});
		textField.setBounds(212, 86, 110, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		textField.setText("0");
		
		lblPressure = new JLabel("Pressure:");
		lblPressure.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		lblPressure.setBounds(116, 358, 68, 20);
		frame.getContentPane().add(lblPressure);
		
		label = new JLabel("");
		label.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		label.setBounds(185, 358, 288, 20);
		frame.getContentPane().add(label);
		
		lblTemperature = new JLabel("Temperature: ");
		lblTemperature.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		lblTemperature.setBounds(116, 383, 117, 20);
		frame.getContentPane().add(lblTemperature);
		
		label_1 = new JLabel("");
		label_1.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		label_1.setBounds(215, 383, 281, 20);
		frame.getContentPane().add(label_1);
		
		lblDensity = new JLabel("Density:");
		lblDensity.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		lblDensity.setBounds(116, 408, 68, 20);
		frame.getContentPane().add(lblDensity);
		
		label_2 = new JLabel("");
		label_2.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		label_2.setBounds(178, 408, 282, 20);
		frame.getContentPane().add(label_2);
		
		lblAltitude = new JLabel("Altitude:");
		lblAltitude.setFont(new Font("Cambria", Font.BOLD | Font.ITALIC, 15));
		lblAltitude.setBounds(116, 89, 68, 14);
		frame.getContentPane().add(lblAltitude);
		
		lblAtomsphericCalculator = new JLabel("Standard Atmosphere Calculator");
		lblAtomsphericCalculator.setFont(new Font("Cambria", Font.BOLD, 30));
		lblAtomsphericCalculator.setBounds(37, 11, 485, 51);
		frame.getContentPane().add(lblAtomsphericCalculator);
		
		lblAt = new JLabel("At ");
		lblAt.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		lblAt.setBounds(116, 333, 25, 14);
		frame.getContentPane().add(lblAt);
		
		lblAltLabel = new JLabel("");
		lblAltLabel.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		lblAltLabel.setBounds(139, 333, 260, 14);
		frame.getContentPane().add(lblAltLabel);
		
		lblNewLabel = new JLabel("At sea level");
		lblNewLabel.setFont(new Font("Cambria", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel.setBounds(116, 192, 80, 20);
		frame.getContentPane().add(lblNewLabel);
		
		lblPressurepa = new JLabel("Pressure: 101325 Pa");
		lblPressurepa.setFont(new Font("Cambria", Font.BOLD, 15));
		lblPressurepa.setBounds(116, 216, 146, 14);
		frame.getContentPane().add(lblPressurepa);
		
		lblTemperaturek = new JLabel("Temperature: 288.15 K");
		lblTemperaturek.setFont(new Font("Cambria", Font.BOLD, 15));
		lblTemperaturek.setBounds(116, 236, 171, 23);
		frame.getContentPane().add(lblTemperaturek);
		
		lblDensityKgm = new JLabel("Density: 1.225 kg/m^3");
		lblDensityKgm.setFont(new Font("Cambria", Font.BOLD, 15));
		lblDensityKgm.setBounds(116, 260, 171, 20);
		frame.getContentPane().add(lblDensityKgm);
		
		lblSpeedOfSound = new JLabel("Speed of sound: 340.296436 m/s");
		lblSpeedOfSound.setFont(new Font("Cambria", Font.BOLD, 15));
		lblSpeedOfSound.setBounds(116, 284, 234, 20);
		frame.getContentPane().add(lblSpeedOfSound);
		
		lblSpeedOfSound_1 = new JLabel("Speed of sound: ");
		lblSpeedOfSound_1.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		lblSpeedOfSound_1.setBounds(116, 431, 127, 20);
		frame.getContentPane().add(lblSpeedOfSound_1);
		
		speedOfsound = new JLabel("");
		speedOfsound.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 15));
		speedOfsound.setBounds(238, 431, 267, 23);
		frame.getContentPane().add(speedOfsound);
		
		
	}
	private void appCal()
	{   
		// Reading the altitude type and units
		if(rdbtnM.isSelected())
		{
			 h=Double.parseDouble(textField.getText());
			 un="m";
			 //s =textField.getText();	 	
		}
		if(rdbtnFt.isSelected())
		{
			 h=Double.parseDouble(textField.getText())*0.3048;
			 un="ft";
			 //s =textField.getText();	
		}
		if(rdbtnGeometric.isSelected())
		{ 
			h=Re*h/(Re+h);
			 //s =textField.getText();	
		}
		
	       // calculations
	       t1=t0+a1*11000;
	       t2=t1;
	       t3=t2+a2*12000;
	       t4=t3+a3*15000;
	       t5=t4;
	       t6=t5+a4*20000;
	       
	       p1=p0*Math.pow(t1/t0,-g/(R*a1));
	       p2=p1*Math.exp(-g*9000/(R*t2));
	       p3=p2*Math.pow(t3/t2,-g/(R*a2));
	       p4=p3*Math.pow(t4/t3,-g/(R*a3));
	       p5=p4*Math.exp(-g*4000/(R*t4));
	       p6=p5*Math.pow(t6/t5,-g/(R*a4));
	       
	       
	       d1=d0*Math.pow(t1/t0,-g/(R*a1)-1);
	       d2=d1*Math.exp(-g*9000/(R*t2));
	       d3=d2*Math.pow(t3/t2,-g/(R*a2)-1);
	       d4=d3*Math.pow(t4/t3,-g/(R*a3)-1);
	       d5=d4*Math.exp(-g*4000/(R*t4));
	       d6=d5*Math.pow(t6/t5,-g/(R*a4)-1);
	       
	      
	       if(h>=0&&h<=11000)
	       {
	    	   T=288.15+a1*h;
	    	   P=p0*Math.pow(T/t0,-g/(R*a1));
	    	   D=d0*Math.pow(T/t0,-g/(R*a1)-1);
	    	   a=Math.sqrt(gm*R*T);
	       }
	       if(h>11000&&h<=20000)
	       {
	    	  T=t2;
	    	  P=p1*Math.exp(-g*(h-11000)/(R*t2));
	    	  D=d1*Math.exp(-g*(h-11000)/(R*t2));
	   	      a=Math.sqrt(gm*R*T);
	       }
	       if(h>20000&&h<=32000)
	       {
	    	  T=t2+a2*(h-20000);
	    	  P=p2*Math.pow(T/t2,-g/(R*a2));
	    	  D=d2*Math.pow(T/t2,-g/(R*a2)-1);
	   	      a=Math.sqrt(gm*R*T);
	       }
	       if(h>32000&&h<=47000)
	       {
	    	   T=t3+a3*(h-32000);
	    	   P=p3*Math.pow(T/t3,-g/(R*a3));
	    	   D=d3*Math.pow(T/t3,-g/(R*a3)-1);
	    	   a=Math.sqrt(gm*R*T);
	       }
	       if(h>47000&&h<=51000)
	       { 
	    	   
	    	   T=t4;
	    	   P=p4*Math.exp(-g*(h-47000)/(R*t4));
		       D=d4*Math.exp(-g*(h-47000)/(R*t4));
		   	   a=Math.sqrt(gm*R*T);
	       }
	       if(h>51000&&h<=71000)
	       {
	    	   T=t5+a4*(h-51000);
	    	   P=p5*Math.pow(T/t5,-g/(R*a4));
	    	   D=d5*Math.pow(T/t5,-g/(R*a4)-1);
	    	   a=Math.sqrt(gm*R*T);
	       }
	       if(h>71000&&h<=84852)
	       {
	    	   T=t6+a5*(h-71000);
	    	   P=p6*Math.pow(T/t6,-g/(R*a5));
	    	   D=d6*Math.pow(T/t6,-g/(R*a5)-1);
	    	   a=Math.sqrt(gm*R*T);
	       }
	       
	       p=String.valueOf(P);
	   	   t=String.valueOf(T);
	   	   d=String.valueOf(D);
	       ss=String.valueOf(a);
	       
	       if(h>84845)
	       { 
	    	   p=NA;
	    	   t=NA;
	    	   d=NA;
	    	   ss=NA;
	       }	 
	       
	       // displaying results on textfields
	       lblAltLabel.setText(textField.getText()+un);
	       label.setText(p+" Pa");
	       label_1.setText(t+" K");
	       label_2.setText(d+" Kg/m^3");
	       speedOfsound.setText(ss+" m/s");
	}       
}